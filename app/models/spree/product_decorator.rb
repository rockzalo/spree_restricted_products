Spree::Product.class_eval do
  add_search_scope :only_public do |attr|
    where(restricted: [false, nil])
  end
end