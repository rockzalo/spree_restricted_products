Spree::ProductsController.class_eval do
  include RestrictProductsForUsers

  private

  def load_product
    if try_spree_current_user.try(:has_spree_role?, "admin")
      @products = Spree::Product.with_deleted
    elsif try_spree_current_user
      @products = Spree::Product.active(current_currency)
    else
      @products = Spree::Product.active(current_currency).only_public(true)
    end
    @product = @products.friendly.find(params[:id])
  end
end