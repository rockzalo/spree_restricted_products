Spree::TaxonsController.class_eval do
  before_action :restrict_products_unless_user, only: :show

  def restrict_products_unless_user
    params.merge!({search: {only_public: true }}) unless try_spree_current_user
  end
end