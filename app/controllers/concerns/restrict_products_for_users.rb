module RestrictProductsForUsers
  extend ActiveSupport::Concern

  included do
    before_action :restrict_products_unless_user, only: :index
  end

  def restrict_products_unless_user
    params.merge!({search: {only_public: true }}) unless try_spree_current_user
  end
end
