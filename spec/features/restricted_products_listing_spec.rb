require "spec_helper"

RSpec.feature "Restricted products listing", :type => :feature do
  let(:store_name) do
    ((first_store = Spree::Store.first) && first_store.name).to_s
  end

  let(:normal_product) { create(:product) }
  let(:restricted_product) { create(:product, restricted: true) }

  context "Guest user" do
    before(:each) do
      visit spree.root_path
    end

    scenario 'search a public product' do
      fill_in "keywords", with: normal_product.name
      click_button "Search"

      expect(page.all('#products .product-list-item').size).to eq(1)
    end

    scenario 'search a restricted product' do
      fill_in "keywords", with: restricted_product.name
      click_button "Search"

      expect(page.all('#products .product-list-item').size).to eq(0)
    end
  end

  context "Logged user" do
    before do
      user = create(:user)
      allow_any_instance_of(Spree::HomeController).to receive_messages(try_spree_current_user: user)
      allow_any_instance_of(Spree::ProductsController).to receive_messages(try_spree_current_user: user)
      visit spree.root_path
    end

    scenario 'search a public product' do
      fill_in "keywords", with: normal_product.name
      click_button "Search"

      expect(page.all('#products .product-list-item').size).to eq(1)
    end

    scenario 'search a restricted product' do
      fill_in "keywords", with: restricted_product.name
      click_button "Search"

      expect(page.all('#products .product-list-item').size).to eq(1)
    end
  end
end