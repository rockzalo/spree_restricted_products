class AddRestrictedToSpreeProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :restricted, :boolean
  end
end
